cc = gcc
app = bin/mekvs
obj = src/sqlite3.o src/mekvs.o 
includepath = -I"./include"
libpath = -L"./lib_centos64"
uselib = -levent -levent_core -levent_extra -levent_openssl -levent_pthreads -lm -lz -ldl -lpthread -lrt

$(app): $(obj)
	$(cc) -o $(app) $(libpath) $(obj) $(uselib)

%.o: %.c
	$(cc) $(includepath) -c $< -o $@

clean:
	rm -rf $(app) $(obj)
